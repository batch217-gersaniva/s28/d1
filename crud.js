// CRUD Operations
// C - Create
// R - Read
// U - Update
// D - Delete

// SECTION - Create (Insert documents)

/*

Syntax: db.collectionName.insertOne({object});
*/ 

db.users.insertOne({
	firstName: "Jane",
	lastName: "Doe",
	age: 21,
	contact: {
		phone: "87654321",
		email: "janedoe@mail.com"
	},
	courses:["CSS", "JavaScript", "Python"],
	department: "none"
});

// Insert Many

db.users.insertMany([
	{
		firstName: "Stephen",
		lastName: "Hawking",
		age: 76,
		contact: {
		phone: "87654321",
		email: "stephenhawking@gmail.com"
	},
	courses:["Python", "React", "PHP"],
	department: "none"
	},

	{
		firstName: "Neil",
		lastName: "Armstrong",
		age: 82,
		contact: {
		phone: "87654321",
		email: "neilarmstrong@gmail.com"
	},
	courses:["React", "Laravel", "Sass"],
	department: "none"
	}

	]);


// [SECTION] - READ (Finding Documents)

/*

db.collectionName.find();
db.collectionName.find({field:value});

*/

db.users.find();


//finding documents with specific
db.users.find({firstName: "Stephen"});

// finding documents with multiple parameter

db.users.find({lastName: "Armstrong", age: 82});


// SECTION - Update

db.users.insertOne({
	firstName: "Test",
	lastName: "Test",
	age: 0,
	contact: {
		phone: "000000",
		email: "test@gmail.com"
	},
	courses:[],
	department: "none"
});

db.users.updateOne(
	{firstName: "Test"},
	{
		$set: {
			firstName: "Bill",
			lastName: "Gates",
			age: 65,
			contact: {
		phone: "12345678",
		email: "bill@gmail.com"
	},
	courses:["PHP", "Laravel", "HTML"],
	department: "Operations",
	status: "active"
		}
	}
);

// Find
db.users.find({firstName: "Bill"});


// Updating Multiple Documents
// Syntax: db.collectionName.updateMany( {criteria}, {$set: {field:value}} );





db.users.updateMany(
	{department: "none"},
	{ 
		$set : {department: "HR"}
	}
);




db.users.insertOne({
	firstName: "Test",
	lastName: "Test",
	age: 0,
	contact: {
		phone: "000000",
		email: "test@gmail.com"
	},
	courses:[],
	department: "none"
});


db.users.updateMany(
	{firstName: "Test"},
	{ 
		$set : {firstName: "April",
		lastName: "ng",
		age: 99,
		contact: {
			phone: "09123456789",
			email: "aprilng@gmail.com"
		},
		courses:["HTML", "CSS", "JavaScript"],
		department: "Web Dev"}
		}
);


db.users.find({firstName: "Apil"});

//SECTION - Delete
// CAUTION: Be very carefull in deleting documents


//Creating a document to delete

db.users.insertOne({
	firstName: "Test",
	lastName: "Test",
	age: 0,
	contact: {
		phone: "000000",
		email: "test@gmail.com"
	},
	courses:[],
	department: "none"
});


//Dele

db.users.deleteOne({firstName: "Test"});




db.users.insertOne({
		firstName: "Stephen",
		lastName: "Hawking",
		age: 76,
		contact: {
		phone: "87654321",
		email: "stephenhawking@gmail.com"
			},
	courses:["Python", "React", "PHP"],
	department: "none"
	});

db.users.deleteMany({firstName: "Stephen"});





// Replace one 

db.users.replaceOne(
	{ firstName: "Bill" },
	{ firstName: "Mark"
	}
);


// Query an embedded document

db.users.find({
	contact: {
		phone: "87654321",
		email: "stephenhawking@gmail.com"
	}
});


//Query on Nested field

db.users.find(
	{"contact.email": "janedoe@mail.com"}
	);


//Querying an array with exact elements
db.users.find(
	{courses: ["CSS", "JavaScript", "Python"]}
	);


// Querying an array without regards to order

db.users.find({course: {$all: ["JavaScript", "CSS", "Python"]}});



Querying an embedded array

db.users.insertOne({
    namearr: [
        {
            namea: "juan"
        },
        {
            nameb: "tamad"
        }
    ]
});


db.users.find({
	namearr:
	{namea: "juan"}
});